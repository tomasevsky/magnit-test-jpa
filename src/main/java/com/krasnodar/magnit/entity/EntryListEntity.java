package com.krasnodar.magnit.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Author: Oleg Tomashevsky
 * Date: 18.03.2015
 * Time: 18:59
 */
@XmlRootElement(name = "entries")
@XmlAccessorType(XmlAccessType.FIELD)
public class EntryListEntity {
    @XmlElement(name = "entry")
    private Collection<EntryEntity> entries;

    public void setEntries(Collection<EntryEntity> entries) {
        this.entries = entries;
    }
}

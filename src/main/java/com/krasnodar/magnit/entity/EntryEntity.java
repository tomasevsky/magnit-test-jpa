package com.krasnodar.magnit.entity;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Author: Oleg Tomashevsky
 * Date: 18.03.2015
 * Time: 16:54
 */
@Entity
@Cacheable(value = false)
@Table(name = "ENTRY")
@XmlAccessorType(XmlAccessType.FIELD)
public class EntryEntity {

    @Id
    @XmlElement
    private Integer field;

    public void setField(Integer field) {
        this.field = field;
    }
}

package com.krasnodar.magnit;

import com.krasnodar.magnit.entity.EntryEntity;
import com.krasnodar.magnit.entity.EntryListEntity;
import com.krasnodar.magnit.repository.EntryRepository;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Author: Oleg Tomashevsky
 * Date: 23.03.2015
 * Time: 12:08
 */
public class MainBean {

    @Autowired
    EntryRepository repository;

    private Integer n;

    public long executeAll() throws JAXBException, TransformerException, IOException, SAXException {

//        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();


        generateAndSaveData();

        File file1 = new File("1.xml");
        generateXmlFromData(file1);

        File file2 = new File("2.xml");
        transformWithXslt(file1, file2);

        return parseAndFindSum(file2);
    }

    public void generateAndSaveData() {
        repository.deleteAll();
        ArrayList<EntryEntity> list = new ArrayList<EntryEntity>();
        for (int i = 1; i <= n; i++) {
            EntryEntity entryEntity = new EntryEntity();
            entryEntity.setField(i);
            list.add(entryEntity);
        }
        repository.save(list);
    }

    public void generateXmlFromData(File file) throws JAXBException {
        Iterable<EntryEntity> result = repository.findAll();
        EntryListEntity entryListEntity = new EntryListEntity();
        entryListEntity.setEntries(toCollection(result));

        JAXBContext jaxbContext = JAXBContext.newInstance(EntryListEntity.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(entryListEntity, file);
    }

    public void transformWithXslt(File sourceFile, File destFile) throws TransformerException, IOException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(new FileReader(this.getClass().getClassLoader().getResource("transform.xslt").getFile()));
        Transformer transformer = factory.newTransformer(xslt);

        Source source = new StreamSource(sourceFile);
        transformer.transform(source, new StreamResult(destFile));
    }

    public long parseAndFindSum(File file) throws IOException, SAXException {
        long sum = 0;
        DOMParser parser = new DOMParser();

        FileInputStream inputStream = new FileInputStream(file);
        parser.parse(new InputSource(inputStream));

        Document doc = parser.getDocument();
        doc.normalizeDocument();
        doc.getDocumentElement().normalize();
        Element root = doc.getDocumentElement();
        NodeList elements = root.getElementsByTagName("entry");
        for (int i = 0; i < elements.getLength(); i++) {
            Node entryNode = elements.item(i);
            Node fieldAttr = entryNode.getAttributes().getNamedItem("field");
            String fieldValue = fieldAttr.getNodeValue();
            sum += Integer.valueOf(fieldValue);
        }
        return sum;
    }

//    public void setRepository(EntryRepository repository) {
//        this.repository = repository;
//    }

    public void setN(Integer n) {
        this.n = n;
    }

    private static Collection<EntryEntity> toCollection(Iterable<EntryEntity> iterable) {
        if (iterable instanceof Collection) {
            return (Collection<EntryEntity>) iterable;
        }
        List<EntryEntity> list = new LinkedList<EntryEntity>();
        for (EntryEntity entry : iterable) {
            list.add(entry);
        }
        return list;
    }
}

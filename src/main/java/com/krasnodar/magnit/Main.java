package com.krasnodar.magnit;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * Author: Oleg Tomashevsky
 * Date: 18.03.2015
 * Time: 12:36
 */
public class Main {
    public static void main(String[] args) throws JAXBException, TransformerException, IOException, SAXException {
        // check that user entered number of entries argument
        if (args.length != 1) {
            System.err.println("Please, give command as follows : ");
            System.err.println();
            System.err.println("> com.krasnodar.magnit.Main 100");
            System.err.println();
            System.err.println("where '100' is a number of entries");
            return;
        }

        int n = Integer.valueOf(args[0]);

        // init spring context
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-context.xml");
        MainBean mainBean = (MainBean) context.getBean("mainBean");

        mainBean.setN(n);
        long timeStart = System.currentTimeMillis();
        long result = mainBean.executeAll();

        long timeFinish = System.currentTimeMillis();
        long totalTime = timeFinish - timeStart;

        System.out.println("Result: " + result + ", total execution time: " + totalTime / 1000 + "s");
    }
}

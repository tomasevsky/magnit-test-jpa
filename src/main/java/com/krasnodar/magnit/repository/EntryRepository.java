package com.krasnodar.magnit.repository;

import com.krasnodar.magnit.entity.EntryEntity;
import org.springframework.data.repository.CrudRepository;

public interface EntryRepository extends CrudRepository<EntryEntity, Integer> {
}
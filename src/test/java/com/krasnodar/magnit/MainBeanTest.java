package com.krasnodar.magnit;

import com.krasnodar.magnit.entity.EntryEntity;
import com.krasnodar.magnit.repository.EntryRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;

/**
 * Author: Oleg Tomashevsky
 * Date: 18.03.2015
 * Time: 17:53
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class MainBeanTest {
    @Inject
    @Qualifier(value = "mainBean")
    MainBean mainBean;

    @Autowired
    EntryRepository repository;

    private static final Logger logger = LogManager.getLogger(MainBeanTest.class);

    @Test
    public void test2() {

        repository.deleteAll();
        mainBean.setN(100);
        mainBean.generateAndSaveData();
        Iterable<EntryEntity> iterable = repository.findAll();
        Assert.assertNotNull(iterable);
        Assert.assertTrue(iterable.iterator().hasNext());
        Assert.assertEquals(100, ((Collection) iterable).size());

        repository.deleteAll();
        iterable = repository.findAll();
        Assert.assertNotNull(iterable);
        Assert.assertFalse(iterable.iterator().hasNext());
    }

    @Test
    public void test3() throws JAXBException, IOException, SAXException {
        mainBean.setN(3);
        mainBean.generateAndSaveData();

        File dstFile = new File("1.xml");
        mainBean.generateXmlFromData(dstFile);

        XMLUnit.setIgnoreWhitespace(true);
        Diff diff = XMLUnit.compareXML(
                new FileReader(getResourceFile("expected_1.xml")),
                new FileReader(dstFile));
        Assert.assertTrue(diff.similar());
    }

    @Test
    public void test4() throws JAXBException, IOException, TransformerException, SAXException {

        File srcFile = new File(getResourceFile("expected_1.xml"));
        File dstFile = new File("2.xml");
        mainBean.transformWithXslt(srcFile, dstFile);

        XMLUnit.setIgnoreWhitespace(true);
        Diff diff = XMLUnit.compareXML(
                new FileReader(getResourceFile(("expected_2.xml"))),
                new FileReader(dstFile));
        Assert.assertTrue(diff.similar());
    }

    @Test
    public void test5() throws JAXBException, IOException, TransformerException, SAXException {
        File file = new File(getResourceFile("expected_2.xml"));

        long actualSum = mainBean.parseAndFindSum(file);

        long expectedSum = 6;

        Assert.assertEquals(expectedSum, actualSum);
    }

    @Test
    public void test6() throws JAXBException, TransformerException, IOException, SAXException {
        long timeStart = System.currentTimeMillis();

        mainBean.setN(1000000);
        long result = mainBean.executeAll();
        Assert.assertEquals(500000500000L, result);

        long timeFinish = System.currentTimeMillis();
        long totalTime = timeFinish - timeStart;
        logger.info("total time: " + totalTime / 1000 + "s");
        Assert.assertTrue(totalTime < 10 * 60 * 1000);
    }

    private String getResourceFile(String fileName) {
        return this.getClass().getClassLoader().getResource(fileName).getFile();
    }
}